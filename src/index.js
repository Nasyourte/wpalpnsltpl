import Alpine from 'alpinejs'

// JS
console.log('hello')
document.getElementById('h1').innerText += " js"

// Alpinejs
window.Alpine = Alpine
Alpine.start()

// Shoelace
import '@shoelace-style/shoelace/dist/themes/light.css';
import '@shoelace-style/shoelace/dist/components/button/button.js';
import '@shoelace-style/shoelace/dist/components/alert/alert.js';
import '@shoelace-style/shoelace/dist/components/icon/icon.js';
import { setBasePath } from '@shoelace-style/shoelace/dist/utilities/base-path.js';
setBasePath('/shoelace/');